import React, { useState, useEffect } from 'react';
import { Text, View, } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { ButtonGroup, Button } from 'react-native-elements';
import { useDispatch, useSelector } from 'react-redux';
import { getKeyByValue } from '../utils/genericUtils';


const Settings = (props) => {
  const lang = useSelector((state) => state.language.lang);
  const theme = useSelector((state) => state.themes.theme);
  const languages = {
    0 : 'tr',
    1 : 'eng'
  };
  const themes = {
    0 : 'dark',
    1 : 'light'
  };

  const langIndex = getKeyByValue(languages, lang);
  const themeIndex = getKeyByValue(themes, theme);
  const text = lang === 'tr' ? 'AYARLAR' : 'SETTINGS';
  const [selectedLanguage, setSelectedLanguage] = useState(langIndex);
  const [selectedTheme, setSelectedTheme] = useState(themeIndex);
  const uDispatch = useDispatch();
 
  useEffect(() => {
    
    
  });

  return (
    <SafeAreaView style={styles.safeAreaView}>
      <View style={styles.view}>
        <Text>{text}</Text>
        <ButtonGroup
          buttons={['TR', 'ENG']}
          selectedIndex={selectedLanguage}
          onPress={(value) => {
            setSelectedLanguage(value);
            } 
          }
        />
        <ButtonGroup
          buttons={['DARK', 'LIGHT']}
          selectedIndex={selectedTheme}
          onPress={(value) => {
            setSelectedTheme(value);
          }}
        />
        <Button
          title={'APPLY'}
          titleStyle={{ fontWeight: '700'}}
          buttonStyle={{borderRadius:20}}
          containerStyle={{
            width:120,
            position:'absolute',
            bottom:10,
          }}
          onPress={() => {
            uDispatch({type:'LANGUAGE_CHANGE', payload:languages[selectedLanguage]});
            uDispatch({type:'THEME_CHANGE', payload:themes[selectedTheme]});
          }}
        />
      </View>
    </SafeAreaView>


  )

}

const styles = {
  view: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  safeAreaView: {
    flex: 1
  },
  row: {
    flexDirection: 'row'
  }
};

export default Settings;