/* eslint-disable prettier/prettier */
import React, { useEffect, useState, useContext } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AddPatients from './AddPatients';
import Home from './Home';
import ListPatients from './ListPatients';
import Settings from './Settings';
import StartTest from './StartTest';
import LogIn from './LogIn';
import { useSelector, useDispatch } from 'react-redux';
import { AuthContext } from '../context/AuthContext';

const Tab = createBottomTabNavigator();

const Root = ( props ) => {
  const { isLoggedIn, setIsLoggedIn } = useContext(AuthContext);
  const lang =  useSelector((state) => state.language.lang);
  const theme = useSelector((state) => state.themes.theme);

  const routes = {
    'login':{
      'tr' : 'GİRİŞ YAP',
      'eng' : 'LOG IN'
    },
    'home' : {
      'tr' : 'ANA SAYFA',
      'eng': 'HOME'
    },
    'add_patient' : {
      'tr' : 'HASTA EKLE',
      'eng' : 'ADD PATIENT'
    },
    'list_patients' : {
      'tr' : 'HASTA LİSTELE',
      'eng' : 'LIST PATIENTS'
    },
    'start_test' : {
      'tr' : 'TEST BAŞLAT',
      'eng' : 'START TEST'
    },
    'settings' : {
      'tr' : 'AYARLAR',
      'eng' : 'SETTINGS'
    }
  };

  return(
    <NavigationContainer>
      <Tab.Navigator
          backBehavior={'history'}
          screenOptions={({route}) => ({
            tabBarIcon: ({color, size}) => {
            let iconName;

            if (route.name === routes.home[lang]) {
              iconName = 'home';
            } else if (route.name === routes.add_patient[lang]) {
              iconName = 'add-circle';
            } else if (route.name === routes.list_patients[lang]) {
              iconName = 'list-circle';
            } else if (route.name === routes.start_test[lang]) {
              iconName = 'play-circle';
            } else if (route.name === routes.settings[lang]) {
              iconName = 'cog';
            } else if (route.name === routes.login[lang]){
              iconName = 'log-in';
            }
              return <Ionicons name={iconName} size={size} color={color} />;
            },
            tabBarActiveTintColor: 'tomato',
            tabBarInactiveTintColor: 'grey',
          })}

        >
        {
          isLoggedIn ? (
            <>
              <Tab.Screen name={routes.home[lang]} component={Home} />
              <Tab.Screen name={routes.add_patient[lang]} component={AddPatients} />
              <Tab.Screen
                name={routes.list_patients[lang]}
                component={ListPatients}
              />
              <Tab.Screen name={routes.start_test[lang]} component={StartTest} />
              <Tab.Screen name={routes.settings[lang]} component={Settings} />
            </>
          ) : (
            <Tab.Screen name={routes.login[lang]} component={LogIn}/>
          )
        }

        </Tab.Navigator>

    </NavigationContainer>
  )
}

export default Root;