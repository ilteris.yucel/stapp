import React, { useEffect , useState} from 'react';
import { Text, View, } from 'react-native';
import { useSelector } from 'react-redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useDispatch } from 'react-redux';
import * as Progress from 'react-native-progress';


const Home = (props) => {
  const lang = useSelector((state) => state.language.lang);
  const patientList = useSelector((state) => state.patients.patientList);
  const text = lang === 'tr' ? 'ANA SAYFA' : 'HOME';
  const navLink = lang === 'tr' ? 'AYARLAR' : 'SETTINGS';
  const [error, setError] = useState(false);
  const [isLoad, setIsLoad] = useState(false);
  const dispatch = useDispatch();
  useEffect(() => {
    if(patientList.length !== 0){
      setIsLoad(true);
      return;
    }
      
    fetch('https://sheltered-stream-04551.herokuapp.com/get-patients')
    .then((response) => {
      if(response.status !== 200){
        throw new Error('DB Unreached!');
      }
      return response.json();
    })
    .then((json) => {
      setIsLoad(true);
      dispatch({type:'PATIENT_ADD', payload:json});
    })
    .catch((error) => {
      console.log('Error on getting user > ', error);
      setError(true);
      setIsLoad(true);
    })    
  }, []);

  const content = error ? <View style={styles.view}><Text>Error when loading patients</Text></View> :
                          <SafeAreaView style={styles.safeAreaView}>
                            <View style={styles.view}>
                              <Text>{text}</Text>
                            </View>
                          </SafeAreaView>
  const page = isLoad ? content : <View style={styles.view}><Progress.Pie progress={0.4} size={90} color={'red'} indeterminate={true}/></View>
  return (
    <SafeAreaView style={styles.safeAreaView}>
      {page}
    </SafeAreaView>
  );

}

const styles = {
  view: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  safeAreaView: {
    flex: 1
  }
};

export default Home;