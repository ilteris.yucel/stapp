import React, { useState, useEffect } from 'react';
import { Text, View, Modal, Pressable, ScrollView } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Input, Icon } from 'react-native-elements';
import DatePicker from 'react-native-date-picker';
import { Button } from 'react-native-elements'
import Swipper from 'react-native-swiper';

import AddMainHistory from './AddMainHistory';
import AddFamilyHistory from './AddFamilyHistory';
import AddPreHistory from './AddPreHistroy';
import AddNatalHistory from './AddNatalHistory';
import AddPostHistory from './AddPostHistory';
import AddHLHistory from './AddHLHistory';

const AddPatients = (props) => {
  const lang = useSelector((state) => state.language.lang); 
  const patientList = useSelector((state) => state.patients.patientList); 
  const text = lang === 'tr' ? 'HASTA EKLEME SAYFASI' : 'ADDING PATIENT SCREEN';
  const [date, setDate] = useState(new Date());
  const [open, setOpen] = useState(false);
  const [nameInputChange, setNameInputTextChange] = useState(null);
  const [surNameInputChange, setSurNameInputTextChange] = useState(null);
  const [test0LabelChange, setTest0LabelChange] = useState(null);
  const [test1LabelChange, setTest1LabelChange] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);
  const [birthLabelChange, setBirthLabelChange] = useState(date);
  const dispatch = useDispatch();

  // const AddPatients = () => {
  //    fetch('https://sheltered-stream-04551.herokuapp.com/add-patient', {
  //     method: 'POST',
  //     headers: {
  //       'Accept': 'application/json',
  //       'Content-Type': 'application/json'
  //     },
  //     body: JSON.stringify({
  //       name: nameInputChange,
  //       surname: surNameInputChange,
  //       birth: birthLabelChange,
  //       test0: test0LabelChange,
  //       test1: test1LabelChange
  //     })
  //   })
  //   .then((response) => {
  //       console.log(response.status);
  //       if(response.status === 201){
  //         dispatch({type:'PATIENT_ADD', payload:{
  //           id: patientList[patientList.length-1].id+1,
  //           name: nameInputChange,
  //           surname: surNameInputChange,
  //           birth: birthLabelChange,
  //           test0: test0LabelChange ? test0LabelChange : 0,
  //           test1: test1LabelChange ? test1LabelChange : 0
  //         }})
  //         setModalVisible(true);
  //       }
  //   })
  //   .catch((error) => {
  //     console.log(error);
  //   })

  // }
  // const handleChangeNameInput = (text) => {
  //   setNameInputTextChange(text);
  // };

  // const handleChangeSurNameInput = (text) => {
  //   setSurNameInputTextChange(text);
  // };

  // const handleChangeTest0Input = (text) => {
  //   setTest0LabelChange(text);
  // };

  // const handleChangeTest1Input = (text) => {
  //   setTest1LabelChange(text);
  // };

  // useEffect(() => {
  //   //datePicker.current.value = date.toISOString().split('T')[0];
  //   // nameLabelRef.current.value = nameInputChange;
  //   // surnameLabelRef.current.value = surNameInputChange;
  //   // test0LabelRef.current.value = test0LabelChange;
  //   // test1LabelRef.current.value = test1LabelChange;
  // }, [date]);

  return (
    <SafeAreaView style={styles.safeAreaView}>
      <Swipper
        dot={
          <View
            style={{
              backgroundColor: 'rgba(0,0,0,.3)',
              width: 16,
              height: 16,
              borderRadius: 7,
              marginLeft: 7,
              marginRight: 7
            }}
          />
        }
        activeDot={
          <View
            style={{
              backgroundColor: 'red',
              width: 16,
              height: 16,
              borderRadius: 7,
              marginLeft: 7,
              marginRight: 7
            }}
          />
        }
        paginationStyle={{
          bottom: 10
        }}
        loop={false}
        showsButtons={true}
      > 
        <AddMainHistory/>
        <AddFamilyHistory/>
        <AddPreHistory/>
        <AddNatalHistory/>
        <AddPostHistory/>
        <AddHLHistory/>
      </Swipper>
    </SafeAreaView>
  );
}

const styles = {
  view: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  safeAreaView: {
    flex: 1,
  },
  scrollView: {
    backgroundColor: 'transparent',
  },
};

export default AddPatients;