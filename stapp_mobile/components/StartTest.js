import React from 'react';
import { Text, View, } from 'react-native';
import { useSelector } from 'react-redux';
import { SafeAreaView } from 'react-native-safe-area-context';

const StartTest = (props) => {
  const lang = useSelector((state) => state.language.lang); 
  const text = lang === 'tr' ? 'TEST BAŞLATMA PENCERESİ' : 'START TEST SCREEN';

  return (
    <SafeAreaView style={styles.safeAreaView}>
      <View style={styles.view}>
        <Text>{text}</Text>
      </View>
    </SafeAreaView>


  )

}

const styles = {
  view: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  safeAreaView: {
    flex: 1
  }
};

export default StartTest;