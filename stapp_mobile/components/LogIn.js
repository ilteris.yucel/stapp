import React, { useEffect, useState } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Button, Input, Icon } from 'react-native-elements';
import { View } from 'react-native';
import { AuthContext } from '../context/AuthContext';

const LogIn = (props) => {
  const [ userNameInput, setUserNameInput ] = useState(null);
  const [ passwordInput, setPasswordInput] = useState(null);
  const { setIsLoggedIn } = React.useContext(AuthContext);

  const handleChangeUserNameInput = (text) => {
    setUserNameInput(text);
  };

  const handleChangePasswordInput = (text) => {
    setPasswordInput(text);
  };

  const login = () => {
    fetch('https://sheltered-stream-04551.herokuapp.com/auth-user', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username: userNameInput,
        password: passwordInput
      })
    }).then((response) => {
      if(response.status === 200)
        setIsLoggedIn(true);
      else
        console.log('AUTH UNSUCCESSFULL');
    })
    .catch((error) => {
      console.error(error);
    })
  }

  return (
    <SafeAreaView style={styles.safeAreaView}>
      <View style={styles.view}>
        <Input
          placeholder='KULLANICI ADI >'
          leftIcon={{ type: 'font-awesome', name: 'user' }}
          containerStyle={{marginTop:16}}
          onChangeText={handleChangeUserNameInput}
          value={userNameInput}
        />
        <Input 
          placeholder="ŞİFRE >"
          leftIcon={{ type: 'font-awesome', name: 'chevron-right' }} 
          containerStyle={{marginTop:16}}
          onChangeText={handleChangePasswordInput}
          value={passwordInput}
          secureTextEntry={true} 
        />
        <Button
          title="GİRİŞ YAP"
          icon={{
            name: 'arrow-right',
            type: 'font-awesome',
            size: 15,
            color: 'blue',
          }}
          containerStyle={{marginTop:16}}
          onPress={login}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = {
  view: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  safeAreaView: {
    flex: 1,
  },
};

export default LogIn;