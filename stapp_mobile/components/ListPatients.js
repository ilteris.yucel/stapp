import React, { useState, useRef, useEffect }  from  'react';
import { Text, View, FlatList} from 'react-native';
import { useSelector } from 'react-redux';
import { ListItem, Icon, Button } from 'react-native-elements';
import { SafeAreaView } from 'react-native-safe-area-context';
import * as Progress from 'react-native-progress';



const ListPatients = (props) => {
  const lang = useSelector((state) => state.language.lang); 
  const theme = useSelector((state) => state.themes.theme);
  const patientList = useSelector((state) => state.patients.patientList); 
  const path = '../avatars'
  const text = lang === 'tr' ? 'HASTA LİSTELEME SAYFASI' : 'LISTING PATIENT SCREEN';
  //const [patientList, setPatientList] = useState([]);
  const [error, setError] = useState(false);
  const [isLoad, setIsLoad] = useState(false);

  useEffect(() => {

  }, [patientList.length]);

  const renderItem = ({item}) => (
    <ListItem.Swipeable 
      leftContent={
        <Button
          title="UPDATE"
          icon={{ name: 'info', color: 'white' }}
          buttonStyle={{ minHeight: '100%' }}
        />
      }
      rightContent={
        <Button
        title="DELETE"
        icon={{ name: 'delete', color: 'white' }}
        buttonStyle={{ minHeight: '100%', backgroundColor: 'red' }}
      />
      }
      bottomDivider
      topDivider
    >
      <Icon name="user-circle-o" type="font-awesome" color="red" size={48}/>
      <ListItem.Content>
        <ListItem.Title>{item.name + ' ' + item.surname}</ListItem.Title>
        <ListItem.Subtitle>{item.birth}</ListItem.Subtitle>
      </ListItem.Content>
      <ListItem.Content right>
        <ListItem.Title right >{'TEST 1 : ' + item.test0}</ListItem.Title>
        <ListItem.Title right >{'TEST 2 : ' + item.test1}</ListItem.Title> 
      </ListItem.Content> 
      <ListItem.Chevron /> 
    </ListItem.Swipeable>

  );


  return (
    <SafeAreaView style={styles.safeAreaView}>
      <FlatList
        keyExtractor={item => item.id}
        data={patientList}
        renderItem={renderItem}
      >
      </FlatList>
    </SafeAreaView>


  )

}

const styles = {
  view: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  safeAreaView: {
    flex: 1
  }
};

export default ListPatients;