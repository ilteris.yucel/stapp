import { createStore, combineReducers } from 'redux';
import  languageReducer from './reducers/languageReducers';
import settingsReducer from './reducers/settingsReducer';
import themeReducer from './reducers/themeReducers';
import userOperationsReducer from './reducers/userOperationsReducer';

const rootReducers = combineReducers(
  { language: languageReducer,
    settings: settingsReducer,
    themes: themeReducer,
    patients: userOperationsReducer
  }
);

const configureStore = () => {
  return createStore(rootReducers);
}

export default configureStore;