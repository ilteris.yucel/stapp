import { LANGUAGE_CHANGE } from '../constants/languageConstants';

const changeLanguage = (language) => {
  return {
    type: LANGUAGE_CHANGE,
    payload: language
  }
}