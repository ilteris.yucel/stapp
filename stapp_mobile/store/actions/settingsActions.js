import { SETTINGS_CHANGE } from '../constants/settingsConstants';

const changeSettings = (settings) => {
  return {
    type: SETTINGS_CHANGE,
    payload: settings
  }
}