import { SETTINGS_CHANGE } from '../constants/settingsConstants';

const initialState = {
  isChange: false
}
const settingsReducer = (state = initialState, action) => {

  switch(action.type){
    case 'SETTINGS_CHANGE':
      console.log("Settings Change Reducer ", action.payload);
      return{
        ...state,
        isChange: action.payload
      };
    default:
      return state;
  }
};

export default settingsReducer;