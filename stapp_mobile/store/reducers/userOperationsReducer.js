import { PATIENT_ADD, PATIENT_DELETE, PATIENT_UPDATE } from '../constants/userOperationsConstants';

initialState = {
  patientList: []
}
const userOperationsReducer = (state = initialState, action) => {
  switch(action.type) {
    case PATIENT_ADD:
      if(action.payload.constructor.name === 'Array'){
        return{
          ...state,
          patientList: [ ...state.patientList, ...action.payload]
        };
      }
      return{
        ...state,
        patientList: [ ...state.patientList, action.payload ]
      };
    default:
      return state;
  }
}

export default userOperationsReducer;