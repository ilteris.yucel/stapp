import { LANGUAGE_CHANGE } from '../constants/languageConstants';

const initialState = {
  lang: 'tr'
};

const languageReducer = (state = initialState, action) => {
  switch(action.type){
    case LANGUAGE_CHANGE:
      return {
        ...state,
        lang: action.payload
      };
    default:
      return state;
  }
}

export default languageReducer;