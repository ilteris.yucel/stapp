import { THEME_CHANGE } from '../constants/themeConstant';

const initialState = {
  theme: 'dark'
};

const themeReducer = (state = initialState, action) => {
  switch(action.type){
    case THEME_CHANGE:
      return{
        ...state,
        theme: action.payload
      };
      default:
        return state;
  }
}

export default themeReducer;