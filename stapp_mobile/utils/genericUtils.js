const getKeyByValue = (object, value) => {
  return parseInt(Object.keys(object).find(key => object[key] === value));
}

export { getKeyByValue };