/* eslint-disable prettier/prettier */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState, useContext } from 'react';

// import {
//   ScrollView,
//   StatusBar,
//   StyleSheet,
//   Text,
//   View,
// } from 'react-native';

import { SafeAreaProvider } from 'react-native-safe-area-context';
// import { NavigationContainer } from '@react-navigation/native';
// import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
// import { createNativeStackNavigator } from '@react-navigation/native-stack';
// import Ionicons from 'react-native-vector-icons/Ionicons';
// import AddPatients from './components/AddPatients';
// import Home from './components/Home';
// import ListPatients from './components/ListPatients';
// import Settings from './components/Settings';
// import StartTest from './components/StartTest';
import Root from './components/Root';
// import { useSelector, useDispatch } from 'react-redux';
import AuthProvider from './context/AuthContext';;

// const Tab = createBottomTabNavigator();
// const Stack = createNativeStackNavigator();

const App = () => {

  // const lang =  useSelector((state) => state.language.lang);
  // const theme = useSelector((state) => state.themes.theme);

  // const routes = {
  //   'home' : {
  //     'tr' : 'ANA SAYFA',
  //     'eng': 'HOME'
  //   },
  //   'add_patient' : {
  //     'tr' : 'HASTA EKLE',
  //     'eng' : 'ADD PATIENT'
  //   },
  //   'list_patients' : {
  //     'tr' : 'HASTA LİSTELE',
  //     'eng' : 'LIST PATIENTS'
  //   },
  //   'start_test' : {
  //     'tr' : 'TEST BAŞLAT',
  //     'eng' : 'START TEST'
  //   },
  //   'settings' : {
  //     'tr' : 'AYARLAR',
  //     'eng' : 'SETTINGS'
  //   }
  // };

  return (
    <SafeAreaProvider>
      <AuthProvider>
        <Root/>
      </AuthProvider>
      {/* <NavigationContainer>
        <Tab.Navigator
          backBehavior={'history'}
          screenOptions={({route}) => ({
            tabBarIcon: ({color, size}) => {
            let iconName;

            if (route.name === routes.home[lang]) {
              iconName = 'home';
            } else if (route.name === routes.add_patient[lang]) {
              iconName = 'add-circle';
            } else if (route.name === routes.list_patients[lang]) {
              iconName = 'list-circle';
            } else if (route.name === routes.start_test[lang]) {
              iconName = 'play-circle';
            } else if (route.name === routes.settings[lang]) {
              iconName = 'cog';
            }
              return <Ionicons name={iconName} size={size} color={color} />;
            },
            tabBarActiveTintColor: 'tomato',
            tabBarInactiveTintColor: 'grey',
          })}
        >
          <Tab.Screen name={routes.home[lang]} component={Home} />
          <Tab.Screen name={routes.add_patient[lang]} component={AddPatients} />
          <Tab.Screen
            name={routes.list_patients[lang]}
            component={ListPatients}
          />
          <Tab.Screen name={routes.start_test[lang]} component={StartTest} />
          <Tab.Screen name={routes.settings[lang]} component={Settings} />
        </Tab.Navigator>
      </NavigationContainer> */}
    </SafeAreaProvider>
  );
};

// const styles = {
//   view: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center'
//   },
//   safeAreaView: {
//     flex: 1
//   }
// };

export default App;
